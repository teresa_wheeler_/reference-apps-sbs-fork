<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%>
        angular.module('fhirStarter').constant('envInfo',
                {
                    "env": "<%= System.getenv("hspc.reference-apps.env") %>",
                    "securityType": "<%= System.getenv("hsp.platform.api.security.mode") %>"
                });